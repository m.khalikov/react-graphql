import React from 'react'
import { NavLink } from 'react-router-dom';

import classes from './User.module.sass'
import { ReactComponent as Logout } from '../../assets/logout.svg'

const user = props => {
  const { session: { currentUser } } = props

  let name = currentUser.email
  if (currentUser.firstName || currentUser.secondName) name = currentUser.firstName + ' ' + currentUser.secondName

  return (
    <div className={classes.Wrapper}>
      <div className={classes.Aside}>
      </div>
      <div className={classes.Content}>
        <div className={classes.Card}>
          <p>Вы вошли как</p>
          <p className={classes.Name}>{name}</p>
          <NavLink to="/logout">
            <div className={classes.Link}>
              <Logout />
              Выйти
              </div>
          </NavLink>
        </div>
      </div>
    </div>
  )
}
export default user