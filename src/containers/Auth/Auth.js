import React, { useState, useEffect } from 'react'
import Signup from './Signup/Signup'
import Signin from './Signin/Signin'
import classes from './Auth.module.sass'

const Auth = props => {
  const [isSignup, setIsSignup] = useState(false)

  const switchHandler = () => {
    setIsSignup(!isSignup)
  }


  return (
    <div>
      {isSignup ? <Signup /> : <Signin />}
      <p className={classes.Switch} onClick={switchHandler}>
        Switch to
        <b>
          {isSignup ? ' Sign in' : ' Sign up'}
        </b>
      </p>
    </div>
  )
}

export default Auth