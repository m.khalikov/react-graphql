import React from 'react'
import { useQuery } from '@apollo/react-hooks';

import { GET_PROCESSES } from "../../queries";
import classes from './Processes.module.sass'
import ProcessList from '../../components/ProcessList/ProcessList'
import Button from '../../components/UI/Button/Button'
import Spinner from '../../components/UI/Spinner/Spinner'

const Processes = props => {
  const { data, loading, error } = useQuery(GET_PROCESSES)

  if (error) return <div>Error</div>;

  return (
    <div className={classes.Wrapper}>
      <div className={classes.Aside}>
        <h3>Фильтр</h3>
        <Button btnType="Blue" disabled>Добавить фильтр</Button>
      </div>
      <div className={classes.Content}>
        {loading && <Spinner />}
        {data && <ProcessList processes={data.processList || []} />}
      </div>
    </div>
  )
}
export default Processes
