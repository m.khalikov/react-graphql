import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';

import { createStore, combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { Provider } from 'react-redux';

import withSession from './hoc/withSession/withSession'

import './index.css';
import App from './App';

const client = new ApolloClient({
  uri: "/api",
  fetchOptions: {
    credentials: "include"
  },
  request: operation => {
    const token = localStorage.getItem("token");
    operation.setContext({
      headers: {
        Authorization: token
      }
    });
  },
  onError: ({ networkError, graphQLErrors }) => {
    if (networkError) {
      localStorage.setItem("token", "");
    }
  }
});
const rootReducer = combineReducers({
  form: formReducer
})

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

const Root = ({ refetch, session }) => (
  <BrowserRouter>
    <App refetch={refetch} session={session} />
  </BrowserRouter>
);

const RootWithSession = withSession(Root);

const app = (
  <ApolloProvider client={client}>
    <Provider store={store}>
      <RootWithSession />
    </Provider>
  </ApolloProvider>
)

ReactDOM.render(app, document.getElementById('root'));
