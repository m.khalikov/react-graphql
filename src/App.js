import React, { Suspense } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';

import Logout from './containers/Auth/Logout/Logout';
import AppLayout from './hoc/AppLayout/AppLayout';
import AuthLayout from './hoc/AuthLayout/AuthLayout';
import User from './containers/User/User'
import Spinner from './components/UI/Spinner/Spinner';
import Signin from './containers/Auth/Signin/Signin'

const app = props => {

  const { refetch, session } = props
  const isAuthenticated = session && session.currentUser

  const Account = React.lazy(() => {
    return import('./containers/Account/Account');
  });

  const Signup = React.lazy(() => {
    return import('./containers/Auth/Signup/Signup');
  });

  const Processes = React.lazy(() => {
    return import('./containers/Processes/Processes');
  });

  let Layout = AuthLayout
  let routes = (
    <Switch>
      <Route path="/signin" render={props => <Signin {...props} session={session} refetch={refetch} />} />
      <Route path="/signup" render={props => <Signup {...props} session={session} refetch={refetch} />} />
      <Redirect to="/signin" />
    </Switch>
  );

  if (isAuthenticated) {
    Layout = AppLayout
    routes = (
      <Switch>
        <Route path="/account" render={props => <Account {...props} session={session} refetch={refetch} />} />
        <Route path="/" exact render={props => <User {...props} session={session} refetch={refetch} />} />
        <Route path="/logout" render={props => <Logout {...props} session={session} refetch={refetch} />} />
        <Route path="/analytics" render={props => <Processes {...props} />} />
        <Redirect to="/" />
      </Switch>
    );
  }

  return (
    <div>
      <Layout session={session}>
        <Suspense fallback={<Spinner />}>{routes}</Suspense>
      </Layout>
    </div>
  );
}

export default withRouter(app);
