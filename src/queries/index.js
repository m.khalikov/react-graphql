import { gql } from "apollo-boost";

export const GET_CURRENT_USER = gql`
  query {
    currentUser {
      id
      firstName
      secondName
      email
    }
  }
`;

export const GET_PROCESSES = gql`
  query {
    processList {
      id
      name
      numberOfExecutions
      averageLeadTime
      averageActiveTime
      employeesInvolvedProcess
      numberOfScenarios
      start
      end
      loading
    }
  }
`;

export const SIGNIN_USER = gql`
  mutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
    }
  }
`;

export const SIGNUP_USER = gql`
  mutation($email: String!, $password: String!) {
    signup( email: $email, password: $password, firstName: "", secondName: "")
  }
`;

export const UPDATE_USER = gql`
  mutation($id: Int!, $email: String!, $password: String, $firstName: String!, $secondName: String!) {
    editUser(id: $id, email: $email, password: $password, firstName: $firstName, secondName: $secondName) {
      id
    }
  }
`;