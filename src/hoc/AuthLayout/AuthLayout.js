import React from 'react'

import classes from './AuthLayout.module.sass'
import Logo from '../../components/Logo/Logo'

const authLayout = props => {
  return (
    <div className={classes.Layout}>
      <div className={classes.Container}>
        <div className={classes.Logo}>
          <Logo />
        </div>
        <main className={classes.Content}>{props.children}</main>
      </div>
    </div>
  )
}

export default authLayout