import moment from 'moment'
import 'moment/locale/ru'
moment.locale('ru');

const formatDuration = ms => {
  const time = moment.duration(parseInt(ms))
  return `${time.hours()} ч ${time.minutes()} мин `
}
const countPercents = (full, active) => `(${(active / full * 100).toFixed(1)}%)`

const getDateString = ms => moment.unix(parseInt(ms)).format('D MMMM YYYY')

const wordFormatter = (one, two, few, number) => {
  const titles = [one, two, few];
  const cases = [2, 0, 1, 1, 1, 2];

  return number + ' ' + titles[
    number % 100 > 4 && number % 100 < 20
      ? 2
      : cases[number % 10 < 5 ? number % 10 : 5]
  ];
};

const convertAll = ({ numberOfExecutions, averageLeadTime, averageActiveTime, start, end, loading, employeesInvolvedProcess, numberOfScenarios }) => {
  return {
    execCount: numberOfExecutions.toLocaleString('ru-RU'),
    fullTime: formatDuration(averageLeadTime),
    activeTime: formatDuration(averageActiveTime) + countPercents(averageLeadTime, averageActiveTime),
    startDate: getDateString(start),
    endDate: getDateString(end),
    loadingDate: getDateString(loading),
    employees: wordFormatter('сотрудник', 'сотрудника', 'сотрудников', employeesInvolvedProcess),
    scenarios: wordFormatter('сценарий', 'сценария', 'сценариев', numberOfScenarios),
  }
}

export default convertAll