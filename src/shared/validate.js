export const required = value => value ? undefined : 'Заполните поле'
export const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Некорректный адрес' : undefined
export const password = value => value && value.length < 6 ? 'Минимум 6 знаков' : undefined
export const confirm = (value, allValues) => value && value !== allValues.password ? 'Пароли не совпадают' : undefined


const validate = values => {
  const errors = {}
  if (!values.email) {
    errors.email = 'Введите email'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Некорректный email'
  }
  if (!values.password) {
    errors.password = 'Введите пароль'
  } else if (values.password.length < 6) {
    errors.password = 'Введите минимум 6 символов'
  }
  if (!values.confirm) {
    errors.confirm = 'Введите пароль еще раз'
  } else if (values.password !== values.confirm) {
    errors.confirm = 'Пароли не совпадают'
  }
  if (!values.firstName) {
    errors.firstName = 'Введите имя'
  }
  if (!values.secondName) {
    errors.secondName = 'Введите фамилию'
  }
  return errors
}

export default validate