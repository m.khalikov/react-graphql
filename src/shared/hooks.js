import { useState, useEffect } from 'react';

export const useMobile = () => {
  const isClient = typeof window === 'object';
  const [isMobile, setIsMobile] = useState(getSize());

  useEffect(() => {
    if (!isClient) {
      return false;
    }

    const handleResize = () => {
      setIsMobile(getSize());
    }

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  function getSize() {
    return isClient && window.innerWidth <= 768 ? true : false
  }

  return isMobile;
}