import React from 'react';

import classes from './NavigationItems.module.sass';
import NavigationItem from './NavigationItem/NavigationItem';
import { ReactComponent as User } from '../../../assets/user.svg'
import { ReactComponent as Account } from '../../../assets/account.svg'
import { ReactComponent as Analytics } from '../../../assets/analytics.svg'
import { ReactComponent as Logout } from '../../../assets/logout.svg'

const navigationItems = props => {

    const { session: { currentUser } } = props

    let username = '';
    if (currentUser.secondName) username = currentUser.secondName;
    if (currentUser.firstName) username += ' ' + currentUser.firstName[0] + '.';
    if (!username.length) username = currentUser.email;

    return (
        <ul className={classes.NavigationItems}>
            <NavigationItem link="/" exact><User />{username}</NavigationItem>
            <NavigationItem link="/account"><Account />Общие данные</NavigationItem>
            <NavigationItem link="/analytics"><Analytics />Аналитика</NavigationItem>
            <NavigationItem link="/logout"><Logout />Выйти</NavigationItem>
        </ul>
    )
};

export default navigationItems;