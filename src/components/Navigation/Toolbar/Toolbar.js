import React from 'react';
import { useLocation } from "react-router-dom";

import { pagesConfig } from '../../../config'
import classes from './Toolbar.module.sass';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';
import Mapper from '../Mapper/Mapper'
import Search from '../../Search/Search'

const Toolbar = (props) => {
  const location = useLocation()
  const { mapper, search } = pagesConfig[location.pathname]

  return (
    <header className={classes.Toolbar}>
      <DrawerToggle clicked={props.drawerToggleClicked} />
      {mapper && <Mapper mapper={mapper} />}
      {search && <Search search={search} />}
    </header>
  )
};

export default React.memo(
  Toolbar,
  (prevProps, nextProps) =>
    nextProps.location === prevProps.location
);