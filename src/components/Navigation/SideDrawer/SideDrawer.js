import React from 'react';

import NavigationItems from '../NavigationItems/NavigationItems';
import classes from './SideDrawer.module.sass';
import Backdrop from '../../UI/Backdrop/Backdrop';
import { ReactComponent as Menu } from '../../../assets/menu-dark.svg'
import { ReactComponent as Name } from '../../../assets/name.svg'

const sideDrawer = (props) => {
  let attachedClasses = [classes.SideDrawer, classes.Close];
  if (props.open) {
    attachedClasses = [classes.SideDrawer, classes.Open];
  }
  return (
    <>
      <Backdrop show={props.open} clicked={props.closed} />
      <div className={attachedClasses.join(' ')} onClick={props.closed}>
        <div className={classes.Header}>
          <Menu className={classes.Menu} />
          <Name />
        </div>
        <nav className={classes.Nav}>
          <NavigationItems session={props.session} isAuthenticated={props.isAuth} />
        </nav>
      </div>
    </>
  );
};

export default sideDrawer;