import React from 'react'

import convert from '../../../shared/convert'
import classes from './ProcessCard.module.sass'
import Button from '../../UI/Button/Button'
import icons from '../../../shared/icons'

const processCard = props => {
  const data = convert({ ...props })
  return (
    <div className={classes.Card}>
      <div className={classes.Title}>{props.name}
        <Button btnType="Forward" width="auto" disabled withMobile>На карту процесса</Button>
      </div>
      <div className={classes.Body}>
        <div className={classes.Column}>
          <p className={[classes.Value, classes.Main].join(' ')}><img src={icons.reload} alt="" />{data.execCount}</p>
          <p className={classes.Description}>выполнено раз</p>
        </div>
        <div className={classes.Column}>
          <p className={classes.Value}><img src={icons.time} alt="" />{data.fullTime}</p>
          <p className={classes.Description}>среднее время выполнения</p>
          <p className={classes.Value}><img src={icons.averageTime} alt="" />{data.activeTime}</p>
          <p className={classes.Description}>среднее активное время</p>
        </div>
        <div className={classes.Column}>
          <p className={classes.Value}><img src={icons.members} alt="" />{data.employees}</p>
          <p className={classes.Description}>участвует в процессе</p>
          <p className={classes.Value}><img src={icons.script} alt="" />{data.scenarios}</p>
          <p className={classes.Description}>в процессе</p>
        </div>
        <div className={classes.Column}>
          <p className={classes.Description}>Начало</p>
          <p className={classes.ValueRegular}>{data.startDate}</p>
          <p className={classes.Description}>Окончание</p>
          <p className={classes.ValueRegular}>{data.endDate}</p>
          <p className={classes.Description}>Загрузка</p>
          <p className={classes.ValueRegular}>{data.loadingDate}</p>
        </div>
      </div>
    </div>
  )
}

export default processCard