import React from 'react'

import { useMobile } from '../../../shared/hooks'
import classes from './Button.module.sass'

const Button = props => {
  const { width = null } = props
  const isMobile = useMobile()
  const withText = props.withMobile ? !isMobile : true
  return (
    <button className={[classes.Button, classes[props.btnType]].join(' ')} disabled={props.disabled} style={{ width }} onClick={props.clicked}>
      {withText && props.children}
    </button>
  )
}

export default Button