import React from 'react';

import classes from './Input.module.sass';

const input = ({ withLabel, disabled, input, label, type, meta: { touched, error, warning } }) => {

    const inputClasses = [classes.InputElement];

    if (error && touched) {
        inputClasses.push(classes.Invalid);
    }

    const errorClasses = [classes.Error]
    if (withLabel) errorClasses.push(classes.Absolute)

    return (
        <>
            {withLabel && <p className={classes.Description}>{label}</p>}
            <div className={classes.Input}>
                <input
                    className={inputClasses.join(' ')}
                    {...input} placeholder={label}
                    type={type}
                    disabled={disabled}

                />
                {touched && ((error && <span className={errorClasses.join(' ')}>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        </>
    );

};

export default input;