import React from 'react'
import formatError from '../../../shared/errors'
import classes from './Error.module.sass'
import icon from '../../../assets/error.svg'

const error = props => {

  const errors = props.error.graphQLErrors.map(({ message }, i) => (
    <p key={i}>{formatError(message)}</p>
  ))

  return (
    <div className={classes.Wrapper}>
      <img src={icon} alt="" />
      <div>
        {errors}
      </div>
    </div>
  )
}

export default error