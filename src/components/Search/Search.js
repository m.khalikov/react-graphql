import React from 'react'

import classes from './Search.module.sass'
import icon from '../../assets/search.svg'

const search = props => {
  return (
    <div className={classes.Search}>
      <img src={icon} alt="" />
      <input placeholder="Поиск по разделу" />
    </div>
  )
}

export default search