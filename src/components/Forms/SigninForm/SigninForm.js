import React from 'react'
import { Field, reduxForm } from 'redux-form'

import validate from '../../../shared/validate'
import Input from '../../../components/UI/Input/Input'
import Button from '../../../components/UI/Button/Button'

const signinForm = props => {
  const { handleSubmit, submitting } = props

  return (
    <form>
      <Field name="email" type="email" component={Input} label="Электронная почта" />
      <Field name="password" type="password" component={Input} label="Пароль" />
      <Button btnType="Supernova" disabled={submitting} clicked={handleSubmit}>Войти в систему</Button>
    </form>
  )
}

export default reduxForm({
  form: 'signin',
  validate,
})(signinForm)