import React from 'react'
import { Field, reduxForm } from 'redux-form'

import validate from '../../../shared/validate'
import Input from '../../../components/UI/Input/Input'
import Button from '../../../components/UI/Button/Button'

const signupForm = props => {
  const { handleSubmit, submitting } = props

  return (
    <form>
      <Field name="email" type="email" component={Input} label="Электронная почта" />
      <Field name="password" type="password" component={Input} label="Пароль" />
      <Field name="confirm" type="password" component={Input} label="Повторите пароль" />
      <Button btnType="Supernova" disabled={submitting} clicked={handleSubmit}>Войти в систему</Button>
    </form>
  )
}

export default reduxForm({
  form: 'signup',
  validate,
})(signupForm)