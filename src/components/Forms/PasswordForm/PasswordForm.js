import React from 'react'
import { Field, reduxForm } from 'redux-form'

import validate from '../../../shared/validate'
import Input from '../../../components/UI/Input/Input'
import Button from '../../../components/UI/Button/Button'

const passwordForm = props => {
  const { handleSubmit, submitting } = props

  return (
    <>
      <Field name="email" type="email" component={Input} disabled label="Электронная почта" withLabel />
      <Field name="password" type="password" component={Input} label="Введите пароль" withLabel />
      <Field name="confirm" type="password" component={Input} label="Повторите пароль" withLabel />
      <Button btnType="Blue" disabled={submitting} clicked={handleSubmit} width={200}>Сменить пароль</Button>
    </>
  )
}

export default reduxForm({
  form: 'password',
  validate,
})(passwordForm)