export const pagesConfig = {
  '/': {
    mapper: false,
    search: false,
  },
  '/account': {
    mapper: false,
    search: false,
  },
  '/analytics': {
    mapper: {
      img: 'processes',
      title: "Список процессов",
    },
    search: true,
  },
  '/logout': {
    mapper: false,
    search: false,
  },
  '/signin': {
    mapper: false,
    search: false,
  },
  '/signup': {
    mapper: false,
    search: false,
  },
}